import React from 'react';
import { Container, Col, Button, Modal } from 'react-bootstrap';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import './css/Main.css';


//Importamos el constructor

import User from './modules/User';

// Importamos componentes
import Home from './components/Home';
import UserList from './components/UserList';
import NewUser from './components/NewUser';
import EditUser from './components/EditUser';
import DeleteUser from './components/DeleteUser';
import NotFound from './components/NotFound';


class App extends React.Component {

  constructor(props) {
    super(props);

    const defaultUsers = [
      new User(1, "Tiago Oliver", "Freitas", "tiago@example.com"),
      new User(2, "Dolors", "Montserrat", "montse@example.com"),
      new User(3, "Shinozuke", "Nohara", "shinchan@example.com"),
    ];

    this.state = {
      users: defaultUsers,
      lastUser: 3,
      error: '',
      info: '',
      editModal: false,
    };

    this.saveUser = this.saveUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
  }

  saveUser(data) {
    //Si el id=0, asignamos un nuevo ide y actualizamos el state LastUser
    if (data.id === 0) {
      data.id = this.state.lastUser + 1;
      this.setState({ lastUser: data.id,  editModal : data.closeModal });
    }
    // Creamos un nuevo usuario con los datos introducidos por el usuario
    let newUser = new User(data.id, data.nombre, data.apellido, data.email);
    //En una nueva variable, filtramos el state users y traigo todos los usuarios dentro del state a esta nueva variable
    let newUserList = this.state.users.filter(el => el.id !== newUser.id);
    //Envio este nuevo usuario a lista
    newUserList.push(newUser);
    //Digo que esta nueva lista es igual el state users, adicionalmente, envio un booleano para cerrar el modal
    this.setState({ users: newUserList, editModal : data.closeModal });
  }

  deleteUser(idRemove) {
    //En una nueva variable, filtro del state user, el que tenga la id a ser borrada
    let newUserList = this.state.users.filter(el => el.id !== idRemove);
    //Esta lista sin el usuario borrado, vuelve este nuevo valor al state
    this.setState({ users: newUserList });
  }


  render() {
    //
    if (this.state.error.length !== 0) {
      return <div>We encountered an error ,{this.state.error + this.state.info}</div>
    }
    if (this.state.users.length === 0) {
      return <h1>Loading data</h1>
    }
    else {
      return (
        <BrowserRouter>
          <Container className="app-container" fluid={true}>
            <Col md={10} className="app-title-main">
            <h2>Usuarios</h2>
            </Col>
            <Col md={10} className="app-component-display">
              <Switch>
                <Route exact path="/" render={() => <Home data={this.state.users} deleteUser={this.deleteUser} saveUser={this.saveUser} />} />
                <Route path="/list" render={(props) => <UserList users={this.state.users} deleteUser={this.deleteUser} saveUser={this.saveUser} />} />
                <Route path="/newuser" render={() => <NewUser saveUser={this.saveUser} />} />
                <Route path="/editUser/:idUser" render={(props) => <EditUser {...props} users={this.state.users} saveUser={this.saveUser} />} />
                <Route path="/deleteUser/:idUser" render={(props) => <DeleteUser {...props} users={this.state.users} deleteUser={this.deleteUser} />} />
                <Route component={NotFound} />
              </Switch>
            </Col>
            <Col className="app-component-adduser" md={2} >
                <div>
                <Button className="app-button-user" variant="primary" onClick={() => this.setState({ editModal: true})}>
                  <i className="fas fa-plus"></i>
                </Button>
                </div>
                  <Modal show={this.state.editModal} onHide={() => this.setState({ editModal: false})}>
                  <Modal.Header closeButton>
                    <Modal.Title>Nuevo Usuario</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <NewUser saveUser={this.saveUser} />
                  </Modal.Body>
                </Modal>
            </Col>
          </Container>
        </BrowserRouter>
      )
    };
  }
}

export default App;
