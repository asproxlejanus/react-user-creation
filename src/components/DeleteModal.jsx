import React from 'react';
import { Button, Col} from 'react-bootstrap';

import '../css/DeleteUser.css';

class DeleteModal extends React.Component {
    constructor(props) {
        super(props);

        let id = this.props.idUser * 1;
        let currentUser = this.props.users.filter(el => el.id === id);
        let current = currentUser[0];

        this.state = { 
            name : current.nombre,
            apellido : current.apellido,
            email : current.email,
            id : current.id,
            back : false,
         };

        this.deleteUser = this.deleteUser.bind(this);
        this.noDelete = this.noDelete.bind(this);

    }

    deleteUser(value){
        this.props.deleteUser(this.state.id);
        this.props.closeModal(value)
    }

    noDelete(value){
        this.props.closeModal(value);
    }

    render() {  
        return ( 
            <div className="app-delete-user-box">
                <p>¿Estas seguro que deseas borrar el usuario {this.state.name}?</p>
                <Col className="app-delete-buttons-container">
                <Button onClick={()=>this.deleteUser(false)} color="danger">Si</Button>
                <Button onClick={()=>this.noDelete(false)} color="sucess">No</Button>
                </Col>
            </div>
         );
    }
}
 
export default DeleteModal;