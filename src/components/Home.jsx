import React from 'react';
import UserList from './UserList';

class Home extends React.Component {

    render() { 
        return ( 
            <>
            <UserList users={this.props.data} deleteUser={this.props.deleteUser} saveUser={this.props.saveUser}/>
            </>
         );
    }
}
 
export default Home;