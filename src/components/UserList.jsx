import React from 'react';
import { Row, Modal, Button, Col } from 'react-bootstrap';
import DeleteModal from './DeleteModal'
import EditModal from './EditModal'

class UserList extends React.Component {
    constructor(props) {

        super(props);
        this.state = {
            show: false,
            editModal : false,
            deleteModal : false,
        }
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal(value){
        this.setState({
            editModal : value,
            deleteModal : value
        })
    }
    
    render() {
        let rows = this.props.users.sort((a, b) => a.id - b.id).map(user => {
            return (
                <Col key={user.id}>
                    <Col >
                    <Row className="app-user-list">
                    <Col md={1} xs={1}>{user.id}</Col>
                    <Col md={2} xs={2}>{user.nombre}</Col>
                    <Col md={3} xs={3}>{user.apellido}</Col>
                    <Col md={3} xs={3}>{user.email}</Col>
                    <Col md={1} xs={1}>
                        <Button variant="primary" onClick={()=>this.setState({editModal : true, deleteModal : false})}>
                            <i className="far fa-edit"></i>
                        </Button>
                        <Modal show={this.state.editModal} onHide={()=>this.setState({editModal : false, deleteModal : false})}>
                            <Modal.Header closeButton>
                                <Modal.Title>Editar usuario</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <EditModal idUser={user.id} users={this.props.users} saveUser={this.props.saveUser} closeModal={this.closeModal} />
                            </Modal.Body>
                        </Modal>
                    </Col>
                    <Col md={1} xs={1}>
                        <Button variant="primary" onClick={()=>this.setState({editModal : false, deleteModal : true})}>
                            <i className="far fa-trash-alt"></i>
                        </Button>
                        <Modal show={this.state.deleteModal} onHide={()=>this.setState({editModal : false, deleteModal : false})}>
                            <Modal.Header closeButton>
                                <Modal.Title>Borrar usuario</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <DeleteModal idUser={user.id} users={this.props.users} deleteUser={this.props.deleteUser} closeModal={this.closeModal} />
                            </Modal.Body>
                        </Modal>
                    </Col>
                    </Row>
                    </Col>
                    <hr/>
                </Col>
            );
        })
        return (
            <>

            <Col md={12} className="app-user-list-description">
                <Row>
                <Col md={1}>#</Col>
                <Col md={2}>Nombre</Col>
                <Col md={3}>Apellidos</Col>
                <Col md={3}>E-mail</Col>
                </Row>
                <hr/>
            </Col>
                {rows}
            <Col>
            </Col>
            </>
        );
    }
}

export default UserList;