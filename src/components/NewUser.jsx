import React from 'react';
import { Button, Row, Col, Form, FormGroup, FormLabel } from 'react-bootstrap';

import '../css/NewUser.css';

class NewUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            name : '',
            apellido : '',
            email : '',
            nameError : false,
            apellidoError : false,
            emailError : false,
         };

         this.inputChange = this.inputChange.bind(this);
         this.submit = this.submit.bind(this);  
    }

    inputChange(event){
        const v = event.target.value;
        const n = event.target.name;
        this.setState({
            [n] : v
        });
    }

    submit(e){
        e.preventDefault();
        if(this.state.name ===''){
            this.setState({nameError : true});
            return;
        }
        if(this.state.apellido ===''){
            this.setState({apellidoError : true});
            return;
        }
        if(this.state.email ===''){
            this.setState({emailError : true});
            console.log("please insert a valid name")
            return;
        }
        this.props.saveUser({
            nombre : this.state.name,
            apellido : this.state.apellido,
            email : this.state.email,
            id : 0,
            closeModal : false
        });
    }

    render() { 

        let nameError = '';
        let apellidoError = '';
        let emailError = '';

        if(this.state.nameError === true){
            nameError = "error-check-email"
        }
        if(this.state.apellidoError === true){
            nameError = "error-check-email"
        }
        if(this.state.emailError === true){
            nameError = "error-check-email"
        }

        return ( 
            <Col>
            <Form onSubmit={this.submit}>
                <Row>
                    <Col>
                    <FormGroup>
                        <FormLabel inpt="nameInput">Nombre</FormLabel>
                        <input type="text"
                        name="name"
                        id="nameInput"
                        value={this.state.name}
                        onChange={this.inputChange} 
                        />
                        <p className={nameError}>* Este campo es obligatório</p>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel inpt="nameInput">Apellidos</FormLabel>
                        <input type="text"
                        name="apellido"
                        id="apellidoInput"
                        value={this.state.apellido}
                        onChange={this.inputChange} 
                        />
                        <p className={nameError}>* Este campo es obligatório</p>
                    </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                    <FormGroup>
                        <FormLabel inpt="nameInput">E-mail</FormLabel>
                        <input type="email"
                        name="email"
                        id="emailInput"
                        value={this.state.email}
                        onChange={this.inputChange} 
                        />
                        <p className={nameError}>* Este campo es obligatório</p>
                    </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md={12} className="new-user-save-container">
                        <Button type="submit" color="primary">Guardar</Button>
                    </Col>
                    <Col>
                    </Col>
                </Row>
            </Form>
            </Col>
         );
    }
}
 
export default NewUser;