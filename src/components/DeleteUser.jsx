import React from 'react';
import { Button, Col} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';




class DeleteUser extends React.Component {
    constructor(props) {
        super(props);

        let id = this.props.match.params.idUser * 1;
        let currentUser = this.props.users.filter(el => el.id === id);
        let current = currentUser[0];

        this.state = { 
            name : current.nombre,
            apellido : current.apellido,
            email : current.email,
            id : current.id,
            back : false,
         };

        this.deleteUser = this.deleteUser.bind(this);
        this.noDelete = this.noDelete.bind(this);

    }

    deleteUser(){
        this.props.deleteUser(this.state.id);
        this.setState({back : true});
    }

    noDelete(){
        this.setState({back : true});
    }

    render() { 

        if(this.state.back === true){
            return <Redirect to='/list' />
        }

        return ( 
            <div className="app-delete-user-box">
                <p>Are you sure you want to remove this user?</p>
                <Col>
                <Button onClick={this.deleteUser} color="danger">Yes</Button>
                <Button onClick={this.noDelete} color="sucess">No</Button>
                </Col>
            </div>
         );
    }
}
 
export default DeleteUser;