import React from 'react';
import { FormGroup, Form, Row, Col, Button, FormLabel } from 'react-bootstrap';
import { Redirect } from "react-router-dom";

class EditUser extends React.Component {
    constructor(props) {
        super(props);

        let id = this.props.match.params.idUser * 1;
        let userEdit = this.props.users.filter(el => el.id===id)[0]

        this.state = { 
            name : userEdit.nombre,
            apellido : userEdit.apellido,
            email : userEdit.email,
            id : userEdit.id,
            back : false,
         }

        this.inputChange = this.inputChange.bind(this);
        this.submit = this.submit.bind(this);

    }

    inputChange(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        })
    }

    submit(){
        this.props.saveUser({
            nombre: this.state.name,
            apellido : this.state.apellido,
            email : this.state.email,
            id : this.state.id
        });
        this.setState({back : true});
    }

    render() { 

        if(this.state.back === true){
            return <Redirect to='/list'/>
        }
        return ( 
            <Form onSubmit={this.submit}>   
                <Row>
                    <Col>
                        <FormGroup>
                            <FormLabel>Nambe</FormLabel>
                            <input type="text"
                            name="name"
                            id="nameInput"
                            value={this.state.name}
                            onChange={this.inputChange}/>
                        </FormGroup>
                        <FormGroup>
                            <FormLabel>Apellido</FormLabel>
                            <input type="text"
                            name="apellido"
                            id="apellidoInput"
                            value={this.state.apellido}
                            onChange={this.inputChange}/>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <FormLabel>E-mail</FormLabel>
                            <input type="text"
                            name="email"
                            id="emailInput"
                            value={this.state.email}
                            onChange={this.inputChange}/>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button color="primary" type="button" onClick={this.submit}>Save</Button>
                    </Col>
                </Row>
            </Form>
         );
    }
}
 
export default EditUser;